class reverse_iterator:
     def __init__(self, collection):
        self.data = collection
        self.index = len(self.data)
     def __iter__(self):
         return self
     def next(self):
         if self.index == 0:
             raise StopIteration
         self.index = self.index - 1
         return self.data[self.index]

it = reversed(['a', 1, 2, 3, 'c', 17])
type(it)
for each in it:
    print(each)