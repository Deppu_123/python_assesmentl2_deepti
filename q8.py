def fib(n):
    a, b = 0, 1
    for _ in range(n):
        yield a
        a, b = b, a + b


fibonacci_numbers = list(fib(20))
print(fibonacci_numbers)